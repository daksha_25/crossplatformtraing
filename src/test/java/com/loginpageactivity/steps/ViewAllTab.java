package com.loginpageactivity.steps;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;

public class ViewAllTab extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	
	@QAFTestStep(description="user click on leave request tab and user click on view all")
	public void user_click_on_leave_request_tab_and_user_click_on_view_all(){
		CommonStep.waitForNotVisible("page.loader", 30);
		CommonStep.click("link.viewall");
		CommonStep.waitForNotVisible("page.loader", 30);
	}

	@QAFTestStep(description="user click on travel request tab and user click on view all")
	public void user_click_on_travel_request_tab_and_user_click_on_view_all(){
		CommonStep.click("tab.travel");
		CommonStep.waitForNotVisible("page.loader", 30);
		CommonStep.click("link.viewall");
		CommonStep.waitForNotVisible("page.loader", 30);
	}

	@QAFTestStep(description="user click on expence requesttab and user click on view all")
	public void user_click_on_expence_requesttab_and_user_click_on_view_all(){
		CommonStep.click("tab.expence");
		CommonStep.waitForNotVisible("page.loader", 30);
		CommonStep.click("link.viewall");
		CommonStep.waitForNotVisible("page.loader", 30);
		
	}

	@QAFTestStep(description="user able view all leave request accordingly")
	public void user_able_view_all_leave_request_accordingly(){
		CommonStep.assertPresent("txt.leavelist");
		driver.navigate().back();
		CommonStep.waitForNotVisible("page.loader", 30);
		
	}

	@QAFTestStep(description="user able view all travel request accordingly")
	public void user_able_view_all_travel_request_accordingly(){
		CommonStep.assertPresent("txt.travel");
		driver.navigate().back();
		CommonStep.waitForNotVisible("page.loader", 30);
		
	}

	@QAFTestStep(description="user able view all expence request accordingly")
	public void user_able_view_all_expence_request_accordingly(){
		CommonStep.assertPresent("txt.expence");
		driver.navigate().back();
		CommonStep.waitForNotVisible("page.loader", 30);
		
	}
	
	
	


	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
