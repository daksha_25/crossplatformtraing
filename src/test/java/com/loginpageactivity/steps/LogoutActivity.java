package com.loginpageactivity.steps;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;

public class LogoutActivity extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@QAFTestStep(description="user should click on logout button")
	public void user_should_click_on_logout_button(){
		CommonStep.waitForNotVisible("page.loader", 30);
		CommonStep.click("btn.logout.myviewpage");
		CommonStep.waitForNotVisible("page.loader", 30);
		
	}
	@QAFTestStep(description="user logged out successfully")
	public void user_logged_out_successfully(){
		CommonStep.assertPresent("btn.login.homepage");
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}
	

}
