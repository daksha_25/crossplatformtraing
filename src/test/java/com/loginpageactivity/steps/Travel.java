package com.loginpageactivity.steps;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class Travel extends WebDriverBaseTestPage<WebDriverTestPage>{
	@QAFTestStep(description = "user enter username as {0} and password as {1}")
	public void user_enter_username_as____Manager_username___and_password_as____Manager_password__(String username,
			String password) {
		CommonStep.sendKeys(username, "txt.username.homepage");
		CommonStep.sendKeys(password, "txt.password.homepage");
	}

	@QAFTestStep(description = "user click on travel request tab")
	public void user_click_on_travel_request_tab() {
		CommonStep.click("link.travel.managerview");
	}

	@QAFTestStep(description = "user select manager view")
	public void user_select_manager_view() throws InterruptedException {
		CommonStep.waitForNotVisible("page.loader", 30);
		CommonStep.click("slider.view.myviewpage");
		CommonStep.waitForNotVisible("page.loader", 30);

	}

	@QAFTestStep(description = "user click on travel tab and check status")
	public void user_click_on_travel_tab() throws InterruptedException {
		CommonStep.waitForNotVisible("page.loader", 30);
		CommonStep.click("link.travel.managerview");
		CommonStep.waitForNotVisible("page.loader", 30);
		CommonStep.mouseOver("box.visible");
		CommonStep.waitForNotVisible("page.loader", 30);
		CommonStep.click("btn.viewstatus");
		Thread.sleep(3000);

	}

	@QAFTestStep(description = "user can view approve button and reject button and back button")
	public void user_click_on_approve() throws InterruptedException {

		CommonStep.waitForNotVisible("page.loader", 30);
		QAFExtendedWebElement approve = driver.findElement(By.xpath("//button[contains(text(),' Approve')]"));
		Actions action = new Actions(driver);
		action.moveToElement(approve).build().perform();
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,1000)");
		CommonStep.waitForNotVisible("page.loader", 30);
		CommonStep.assertPresent("btn.reject");
		CommonStep.assertPresent("btn.approve");
		CommonStep.assertPresent("btn.Back");
		CommonStep.waitForNotVisible("page.loader", 30);
		
	}

	@QAFTestStep(description = "user able to take actin using buttons")
	public void user_clicked_it_successfully() throws InterruptedException {
		CommonStep.click("btn.approve");
		CommonStep.waitForNotVisible("page.loader", 30);
		CommonStep.assertPresent("btn.submit");
		logger.info("Successfully click on approve button");
	}


	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
