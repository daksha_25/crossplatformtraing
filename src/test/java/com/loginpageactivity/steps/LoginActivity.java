package com.loginpageactivity.steps;

import static org.testng.Assert.assertEquals;

import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class LoginActivity extends WebDriverBaseTestPage<WebDriverTestPage> {
	@QAFTestStep(description = "user navigate to login page of site")
	public void user_navigate_to_login_page() {
		driver.get("/");
		driver.manage().window().maximize();
	}

	@QAFTestStep(description = "user enter username as {0} and password {1}")
	public void user_enter_username_and_password(String username, String password) {
		CommonStep.sendKeys(username, "txt.username.homepage");
		CommonStep.sendKeys(password, "txt.password.homepage");

	}

	@QAFTestStep(description = "user should click on login button")
	public void user_click_on_login_button() throws InterruptedException {
		CommonStep.waitForPresent("btn.login.homepage");
		CommonStep.click("btn.login.homepage");
		CommonStep.waitForNotVisible("page.loader", 30);
	}

	@QAFTestStep(description = "user logged in successfully")
	public void user_logged_in_successfully() {
		CommonStep.assertPresent("txt.profilename.myviewpage");
		
	}

	@QAFTestStep(description = "user enter username as {0} and password {1} both empty")
	public void user_enter_username_as____Blankuser_username___and_password____Blankuser_password___both_empty(
			String username, String password) {

		CommonStep.sendKeys(username, "txt.username.homepage");
		CommonStep.sendKeys(password, "txt.password.homepage");
	}

	@QAFTestStep(description = "user get error message")
	public void user_get_error_message_as__Username_cannot_be_empty_() {

		String errormsg = CommonStep.getText("txt.errormsg.homepage");
		assertEquals(errormsg, "Username is required");
		Validator.verifyThat(CommonStep.getText("txt.errormsg.homepage").equals("Username is required"), Matchers.equalTo(true));

	}

	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub

	}

}
