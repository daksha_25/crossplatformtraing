package com.loginpageactivity.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class LoginTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "txt.username.homepage")
	private QAFWebElement txtUsernameHomepage;
	@FindBy(locator = "txt.password.homepage")
	private QAFWebElement txtPasswordHomepage;
	@FindBy(locator = "btn.login.homepage")
	private QAFWebElement btnLoginHomepage;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getTxtUsernameHomepage() {
		return txtUsernameHomepage;
	}

	public QAFWebElement getTxtPasswordHomepage() {
		return txtPasswordHomepage;
	}

	public QAFWebElement getBtnLoginHomepage() {
		return btnLoginHomepage;
	}

}
